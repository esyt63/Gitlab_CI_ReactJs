import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
          <p> this is on the feature branch </p>
          <p> this is another feature on the feature branch </p>
          <p>feature 1</p>
          <p>parts of feature 1</p>
          <p>testing bugfix merge</p>

      </div>
    );
  }
}

export default App;
