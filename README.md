This project intends to be a basic guide for using Gitlab for automating the building ,testing and deployment of a ReactJs app on AWS S3 bucket.

This project assumes that the [Create React App](https://github.com/facebookincubator/create-react-app) tool is used for the bootstrapping of the  React app project.
Most of the configuration for the  build, testing and deployment of a ReactJs app is already made when Create-react-app tool is used.

Therefore ,the focus of this documentation is not much on ReactJs coding skills but how to use Gitlab CI and have a smooth workflow and automate as much as tasks possible to achieve the best CI/CD practices.

  

To start ,we assume that the project is already been created and push to a Gitlab repo.


In order to start configuring the Gitlab CI the first step is to configure the runner.
This can be done either by registering a new runner or use a shared runner from another project.

The settings and configurations of a runner vary depending on the requirements of the project and the Operating System . More information can be seen [here](http://docs.gitlab.com/runner/).

the first step is to install and register the runner. 

Example steps for the Windows OS would be as follow:

To install and run GitLab Runner on Windows you need:

* Git installed
* A password for your user account, if you want to run it under your user
  account rather than the Built-in System Account

## Installation

1. Create a folder somewhere in your system, ex.: `C:\GitLab-Runner`.
1. Download the binary for [x86][]  or [amd64][] and put it into the folder you
   created. Rename the binary to `gitlab-runner.exe`.
   You can download a binary for every available version as described in
   [Bleeding Edge - download any other tagged release](bleeding-edge.md#download-any-other-tagged-release).
1. Run an [`Administrator`/elevated command prompt][prompt] (<kbd>WindowsKey</kbd> + <kbd>X</kbd> then select Command Prompt (Admin)).
1. [Register the Runner](../register/index.md).
1. Install the Runner as a service and start it. You can either run the service
   using the Built-in System Account (recommended) or using a user account.

    **Run service using Built-in System Account**

    ```bash
    gitlab-runner install
    gitlab-runner start
    ```

    **Run service using user account**

    You have to enter a valid password for the current user account, because
    it's required to start the service by Windows:

    ```bash
    gitlab-runner install --user ENTER-YOUR-USERNAME --password ENTER-YOUR-PASSWORD
    gitlab-runner start
    ```



one note to be mindful of is that the token required when registering the runner can be found under the project settings in the CI/CD section. This should not be confused with the private token.

GitLab offers a continuous integration service. If you
add a .gitlab-ci.yml file to the root directory of your repository,
and configure your GitLab project to use a Runner, then each commit or
push, triggers your CI pipeline 


The next step is configure and design the pipeline.



Similar to most CI tools out in the market such as Travis CI ,Circle CI, Gitlab CI uses a YAML file (.gitlab-ci.yml) to define and configure the pipeline.

The .gitlab-ci.yml file tells the GitLab runner what to do. 
Once its added to the repository, on every push to your Git repository, the Runner will automatically start the pipeline and the pipeline will appear under the project's Pipelines page.

.


In basic terms, A pipeline is consisted of several jobs. Each job always must contain at least the script clause. If the jobs need to be run in order, then they need to be defined as stages otherwise the jobs will run parallel.
The structure of the pipeline depends on the type of the workflow, type of the branch or any further requirement that is needed such as sequence of tasks, failure or success of a job ,etc.. 
This can be achieved by a set of keywords that are able to be used for any job.

the list of keywords can be seen in the table below. for more information see [here](#)

| Keyword       | Required | Description |
|---------------|----------|-------------|
| script        | yes      | Defines a shell script which is executed by Runner |
| image         | no       | Use docker image, covered in [Using Docker Images](../docker/using_docker_images.md#define-image-and-services-from-gitlab-ciyml) |
| services      | no       | Use docker services, covered in [Using Docker Images](../docker/using_docker_images.md#define-image-and-services-from-gitlab-ciyml) |
| stage         | no       | Defines a job stage (default: `test`) |
| type          | no       | Alias for `stage` |
| variables     | no       | Define job variables on a job level |
| only          | no       | Defines a list of git refs for which job is created |
| except        | no       | Defines a list of git refs for which job is not created |
| tags          | no       | Defines a list of tags which are used to select Runner |
| allow_failure | no       | Allow job to fail. Failed job doesn't contribute to commit status |
| when          | no       | Define when to run job. Can be `on_success`, `on_failure`, `always` or `manual` |
| dependencies  | no       | Define other jobs that a job depends on so that you can pass artifacts between them|
| artifacts     | no       | Define list of [job artifacts](../../user/project/pipelines/job_artifacts.md) |
| cache         | no       | Define list of files that should be cached between subsequent runs |
| before_script | no       | Override a set of commands that are executed before job |
| after_script  | no       | Override a set of commands that are executed after job |
| environment   | no       | Defines a name of environment to which deployment is done by this job |
| coverage      | no       | Define code coverage settings for a given job |
| retry         | no       | Define how many times a job can be auto-retried in case of a failure |


A typical pipeline would include stages for installation, testing , building and deployment. 
We will go through each stage in detail.

At the start of the Yaml file the type of docker image required for the pipeline can be defined. These docker images can be either sourced from the dockerhub or local resources.

In this project NodeJs is used, the version of the image can be specified by a colon.
For example, Node:6.10.0.

It is good practice to cache directories that may be used in other stages to save time. This can be achieved by using the keyword Cache: with the key of path: that specifies the directory. Further information can be seen [here]( https://docs.gitlab.com/ee/ci/yaml/README.html#cache)

If there are any variables needed to be used in the pipeline they can be defiened at the beginning of the file.
Gitlab provides a list of predefined variables that can be used . The list can be accssesd from this [link]( https://docs.gitlab.com/ee/ci/yaml/README.html#cache)

Once the stages are defined in the file they need to be described.





stages is used to define stages that can be used by jobs. The specification of stages allows for having flexible multi stage pipelines.
The ordering of elements in stages defines the ordering of jobs' execution:
1.	Jobs of the same stage are run in parallel.
2.	Jobs of the next stage are run after the jobs from the previous stage complete successfully.
Let's consider the following example, which defines 3 stages:
stages:
  - build
  - test
  - deploy
1.	First, all jobs of build are executed in parallel.
2.	If all jobs of build succeed, the test jobs are executed in parallel.
3.	If all jobs of test succeed, the deploy jobs are executed in parallel.
4.	If all jobs of deploy succeed, the commit is marked as success.
5.	If any of the previous jobs fails, the commit is marked as failed and no jobs of further stage are executed.
There are also two edge cases worth mentioning:
1.	If no stages are defined in .gitlab-ci.yml, then the build, test and deploy are allowed to be used as job's stage by default.
2.	If a job doesn't specify a stage, the job is assigned the test stage.

artifacts is used to specify a list of files and directories which should be attached to the job after success. You can only use paths that are within the project workspace

You may want to create artifacts only for tagged releases to avoid filling the build server storage with temporary build artifacts.

To create an archive with a name of the current job:
job:
  artifacts:
    name: "$CI_JOB_NAME"
artifacts:when is used to upload artifacts on job failure or despite the failure.
artifacts:when can be set to one of the following values:
1.	on_success - upload artifacts only when the job succeeds. This is the default.
2.	on_failure - upload artifacts only when the job fails.
3.	always - upload artifacts regardless of the job status.
artifacts:expire_in is used to delete uploaded artifacts after the specified time. By default, artifacts are stored on GitLab forever. expire_in allows you to specify how long artifacts should live before they expire, counting from the time they are uploaded and stored on GitLab.

The basis of Review Apps is the dynamic environments which allow you to create a new environment (dynamically) for each one of your branches.
A Review App can then be visible as a link when you visit the merge request relevant to the branch. That way, you are able to see live all changes introduced by the merge request changes. Reviewing anything, from performance to interface changes, becomes much easier with a live environment and as such, Review Apps can make a huge impact on your development flow.
They mostly make sense to be used with web applications, but you can use them any way you'd like.

Simply put, a Review App is a mapping of a branch with an environment as there is a 1:1 relation between them.
The process of adding Review Apps in a workflow would look like:
1.	Set up the infrastructure to host and deploy the Review Apps.
2.	Set up a job in .gitlab-ci.yml that uses the predefined predefined CI environment variable${CI_COMMIT_REF_NAME} to create dynamic environments and restrict it to run only on branches.
3.	Optionally set a job that manually stops the Review Apps.
From there on, you would follow the branched Git flow:
1.	Push a branch and let the Runner deploy the Review App based on the script definition of the dynamic environment job.
2.	Wait for the Runner to build and/or deploy the web app.
3.	Click on the link that's present in the MR related to the branch and see the changes live

In this project, the deployment infrastructure is an Amazon AWS S3 bucket.
A S3 bucket is configured to host a static website. For each branch, a separate folder is created in the bucket that would host the build artifacts of that branch. Once a review is stopped the branch information will be deleted.
 The staging and production have their bucket configured similarly as well but they will host the latest update on those branches.
The AWS access key and secret key are defined as variable in the yml file.
They have been defined as secret keys under the project CI/CD settings.
For the deploying on the AWS, a Python image is required because the AWS CLI is based on Python and it needs to be installed prior to running aws-cli commands.

One thing to note when using a AWS S3 bucket to host a static website is to configure the access level and Bucket policy to allow access to the site. If the bucket policy is not set correctly, an access denied message will be shown when the link is clicked.

For further information ,please refer to the AWS documentation [here]( http://docs.aws.amazon.com/AmazonS3/latest/dev/example-bucket-policies.html). 









